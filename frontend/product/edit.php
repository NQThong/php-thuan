<?php 
    require "../connect.php";

    session_start();

    if(isset($_SESSION['id'])){
        $userId = $_SESSION['id'];
    }
    
    $a = true;
    $error1 = '';
    $error2 = '';
    $error3 = '';
    $error4 = '';
    $dinhDangImg = [
		'jpg', 'png', 'jpeg'	
	];

    $id = $_GET['id'];

    $sql1 = "SELECT * FROM product WHERE id = '$id'";
    $result = $con->query($sql1);
    $getData = $result->fetch_assoc();
    // var_dump($getData);

    if(isset($_POST['btnEdit'])){
        if(empty($_POST['title'])){
            $a = false;
            $error1 = 'Nhap title';
        }

        if(empty($_POST['price'])){
            $a = false;
            $error2 = 'Nhap price';
        }

        if($_FILES['img']['name'] == ""){
            $error3 = 'Upload image';
        }else{
            $getNameImg = $_FILES['img']['name'];
            $cvName = explode('.',$getNameImg);
            if(!(in_array($cvName['1'],$dinhDangImg))){
                $a = false;
                $error4 = 'Ko phai image';
            }
        }

        if($a == true){
            $title = $_POST['title'];
            $price = $_POST['price'];
            $sql = "UPDATE product SET title = '$title', price = '$price', img = '$getNameImg', `user_id` = '$userId' 
            WHERE id = '$id' ";
            if($con->query($sql)){
                move_uploaded_file($_FILES['img']['tmp_name'], './../ImageProduct/'.$_FILES['img']['name']);
                header('Location: list.php');
            }else{
                echo "Error: " . $sql . "<br>" . $con->error;
            }
        }
    }
?>
<form action="#" method="POST" enctype="multipart/form-data">
    <input type="text" name="title" placeholder="Title" value="<?php if(isset($getData)){ echo $getData['title']; } ?>"/>
    <?php echo $error1; ?>
    <br/>
    <input type="text" name="price" placeholder="Price" value="<?php if(isset($getData)){ echo $getData['price']; } ?>"/>
    <?php echo $error2; ?>
    <br/>
    <input type="file" name="img"/>
    <?php echo $error3; ?>
    <?php echo $error4; ?>
    <br/>
    <button type="submit" name="btnEdit" class="btn btn-default">Update</button>
</form>