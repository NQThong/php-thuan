<?php 

    require '../connect.php';
    session_start();

    if(isset($_SESSION['id'])){
        $userId = $_SESSION['id'];
    }
    
    $msg1 = '';
    $sql = "SELECT * FROM user INNER JOIN product ON user.id = product.user_id WHERE product.user_id = '$userId'";

    $result = $con->query($sql);
    // var_dump($result);
    if($result){
        // echo '1';
    }else{
        echo "Error: " . $sql . "<br>" . $con->error;
    }
    $url = "../ImageProduct";
    
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <style type="text/css">
            table{
                width: 800px;
                margin: auto;
                text-align: center;
            }
            tr {
                border: 1px solid;
            }
            th {
                border: 1px solid;
            }
            td {
                border: 1px solid;
            }
            h1{
                text-align: center;
                color: red;
            }
            #button{
                margin: 2px;
                margin-right: 10px;
                float: right;
            }
        </style>
    </head>
    <body>
        <table id="datatable" style="border: 1px solid">
            <h1>Quản lý product</h1>
            <br/>
            <thead>
                <tr role="row">
                    <th>ID</th>
                    <th>Title</th>
                    <th>Price</th>
                    <th>Image</th>
                    <th>Anh</th>
                    <th style="width: 7%;">Edit</th>
                    <th style="width: 10%;">Delete</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if(isset($result)){
                        if($result->num_rows == 0){
                            $msg1 = 'Ko co product';
                        }else{
                            while($row = $result->fetch_assoc()){?>
                                <tr role="row">
                                    <td><?php echo $row['id']?></td>
                                    <td><?php echo $row['title']?></td>
                                    <td>$<?php echo $row['price']?></td>
                                    <td><?php echo $row['img']?></td>
                                    <td><img src="<?php echo $url ?>/<?php echo $row['img']?>" width="50px" height="50px"/></td>
                                    <td><a href="edit.php?id=<?php echo $row['id']?>">Edit</a></td>
                                    <td><a href="delete.php?id=<?php echo $row['id']?>"> Delete</a></td>
                                </tr>';
                            <?php } 
                        }
                    } 
                ?>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="8">
                        <a href="add.php"><button id="button">Thêm product</button></a>
                    </td>
                </tr>
            </tfoot>
        </table>
    </body>
</html>