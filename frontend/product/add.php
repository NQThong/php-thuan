<?php 
    require ("../connect.php");
    session_start();

    if(isset($_SESSION['id'])){
        $userId = $_SESSION['id'];
    }

    $a = true;
    $error1 = '';
    $error2 = '';
    $error3 = '';
    $error4 = '';
    $dinhDangImg = [
		'jpg', 'png', 'jpeg'	
	];
    if(isset($_POST['btnAdd'])){
        if(empty($_POST['title'])){
            $a = true;
            $error1 = 'Nhap title';
        }

        if(empty($_POST['price'])){
            $a = true;
            $error2 = 'Nhap price';
        }

        if($_FILES['img']['name'] == ""){
            $error3 = 'Upload Image';
        }else{
            $getNameImg = $_FILES['img']['name'];
            $cvName = explode('.',$getNameImg);
            if(!(in_array($cvName['1'],$dinhDangImg))){
                $a = false;
                $error4 = 'Ko phai image';
            }
        }

        if($a == true){
            $title = $_POST['title'];
            $price = $_POST['price'];
            $sql = "INSERT INTO product (title, price, img, user_id) VALUES('$title', '$price', '$getNameImg', '$userId')";
            if($con->query($sql)){
                // echo 'Them san pham thanh cong';
				move_uploaded_file($_FILES['img']['tmp_name'], './../ImageProduct/'.$_FILES['img']['name']);
                header('Location: list.php');
			}else{
				echo "Error: " . $sql . "<br>" . $con->error;
			}
        }
    }
?>
<form action="#" method="POST" enctype="multipart/form-data">
    <input type="text" name="title" placeholder="Title"/>
    <?php echo $error1; ?>
    <br/>
    <input type="text" name="price" placeholder="Price"/>
    <?php echo $error2; ?>
    <br/>
    <input type="file" name="img"/>
    <?php echo $error3; ?>
    <?php echo $error4; ?>
    <br/>
    <button type="submit" name="btnAdd" class="btn btn-default">Add Product</button>
</form>