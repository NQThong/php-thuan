<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <style type="text/css">
            table{
                width: 800px;
                margin: auto;
                text-align: center;
            }
            tr {
                border: 1px solid;
            }
            th {
                border: 1px solid;
            }
            td {
                border: 1px solid;
            }
            h1{
                text-align: center;
                color: red;
            }
            #button{
                margin: 2px;
                margin-right: 10px;
                float: right;
            }
        </style>
    </head>
    <body>
        <?php 
            //Kết nối databse
            include 'connect.php';
            
            //Viết câu SQL lấy tất cả dữ liệu trong bảng players
            $sql = "SELECT * FROM `players` ORDER BY `id`";

            //Chạy câu SQL
            $result = $con->query($sql);
            // var_dump($result);
            //if co data thi num_rows > 0, num_rows =0


            // $data = [];
            // if ($result->num_rows > 0) {

            //     //Gắn dữ liệu lấy được vào mảng $data
            //     while ($row = $result->fetch_assoc()) {
            //         // var_dump($row);
            //         $data = $row;
            //     }
            // }
            
            $url = "ImageFile";

            
        ?>
        <table id="datatable" style="border: 1px solid">
            <h1>Quản lý cầu thủ</h1>
            <thead>
                <tr role="row">
                    <th>ID</th>
                    <th>Tên cầu thủ</th>
                    <th>Tuổi</th>
                    <th>Quốc tịch</th>
                    <th>Vị trí</th>
                    <th>Lương</th>
                    <th>Img</th>
                    <th>Anh</th>
                    <th style="width: 7%;">Edit</th>
                    <th style="width: 10%;">Delete</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    while($row = $result->fetch_assoc()){?>
                        <tr role="row">
                    <td><?php echo $row['id']?></td>
                    <td><?php echo $row['name']?></td>
                    <td><?php echo $row['age']?></td>
                    <td><?php echo $row['national']?></td>
                    <td><?php echo $row['position']?></td>
                    <td><?php echo $row['salary']?></td>
                    <td><?php echo $row['img']?></td>
                    <td><img src="<?php echo $url ?>/<?php echo $row['img']?>" width="50px" height="50px"/></td>
                    <td><a href="edit.php?id=<?php echo $row['id']?>">Edit</a></td>
                    <td><a href="delete.php?id=<?php echo $row['id']?>"> Delete</a></td>
                </tr>';
                    <?php } 
                ?>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="8">
                        <a href="add.php"><button id="button">Thêm cầu thủ</button></a>
                    </td>
                </tr>
            </tfoot>
        </table>
    </body>
</html>