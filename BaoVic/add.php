<?php 
     include 'connect.php';
     // var_dump($con);
     // echo '1';
     $a = true;
     $error1 = '';
     $error2 = '';
     $error3 = '';
     $error4 = '';
     $error5 = '';
     $error6 = '';
     $error7 = '';
     $arrImg = [
          'png', 'jpg', 'jpeg'
     ];

     if(isset($_POST['btnSubmit'])){
          if(empty($_POST['name1'])){
               $a = false;
               $error1 = 'Nhap name';
          }

          if(empty($_POST['age'])){
               $a = false;
               $error2 = 'Nhap age';
          }

          if(empty($_POST['national'])){
               $a = false;
               $error3 = 'Nhap national';
          }

          if(empty($_POST['position'])){
               $a = false;
               $error4 = 'Nhap position';
          }

          if(empty($_POST['salary'])){
               $a = false;
               $error5 = 'Nhap salary';
          }

          if($_FILES['image']['name'] == ""){
               $error6 = 'Upload image';
          }else{
               $getNameImg = $_FILES['image']['name'];
               $tachName = explode('.',$getNameImg);
               // var_dump($tachName['1']);
               if(!(in_array($tachName['1'],$arrImg))){
                    $error7 = 'Ko phai anh';
               }
          }

          if($a == true){
               $name = $_POST['name1'];
               $age = $_POST['age'];
               $national = $_POST['national'];
               $position = $_POST['position'];
               $salary = $_POST['salary'];
               move_uploaded_file($_FILES['image']['tmp_name'], './ImageFile/'.$_FILES['image']['name']);
               //Chạy câu SQL
               $sql = "INSERT INTO players (name, age, national, position, salary, img) 
               VALUES ('$name', '$age', '$national', '$position', '$salary', '$getNameImg')";

               if ($con->query($sql)) {
                    // var_dump($con->query($sql));
                    echo "<h1>Thêm mới cầu thủ thành công Click vào <a href='index.php'>đây</a> để về trang danh sách</h1>";
               }else{
                    // var_dump($con->query($sql));
                    // var_dump($sql);
                    // echo "Error: " . $sql . "<br>" . $con->error;
                    echo "<h1>Có lỗi xảy ra Click vào <a href='index.php'>đây</a> để về trang danh sách</h1>";
               }
          }
     }
?>

<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Add Page</title>
</head>
<body>
     <form action="" method="POST" enctype="multipart/form-data">
          Ten:
          <input type="text" name="name1">
          <?php echo $error1; ?>
          <br/>
          Tuoi:
          <input type="text" name="age">
          <?php echo $error2; ?>
          <br/>
          Quoc tich:
          <input type="text" name="national">
          <?php echo $error3; ?>
          <br/>
          Vi tri:
          <input type="text" name="position">
          <?php echo $error4; ?>
          <br/>
          Luong:
          <input type="text" name="salary">
          <?php echo $error5; ?>
          <br/>
          Anh:
          <input type="file" name="image">
          <?php echo $error6; ?>
          <?php echo $error7; ?>
          <br/>
          <input type="submit" name="btnSubmit">
     </form>
</body>
</html>