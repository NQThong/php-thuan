<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        $error1 = '';
        $error2 = '';
        $error3 = ''; 
        if(isset($_POST['submit'])){
            $demo = true;
            if(empty($_POST['toan'])){
                $error1 = 'Nhap toan';
                $demo = false;
            }

            if(empty($_POST['ly'])){
                $error2 = 'Nhap ly';
                $demo = false;
            }

            if(empty($_POST['hoa'])){
                $error3 = 'Nhap hoa';
                $demo = false;
            }

            if($demo == true){
                $toan = $_POST['toan'];
                $ly = $_POST['ly'];
                $hoa = $_POST['hoa'];
                if(($toan == 1) || ($ly == 1) || ($hoa == 1)){
                    echo 'Yeu vi mot trong ba mon co diem 1';
                }else{
                    $all = $toan + $ly + $hoa; 
                    if($all >= 15){
                        echo 'Pass';
                    }else{
                        echo 'Fail';
                    }
                }
            }
        }
    ?>
    <form action="" method="POST">
        Toan: <input type="text" name="toan">
        <p><?php echo $error1; ?></p>
        Ly: <input type="text" name="ly">
        <p><?php echo $error2; ?></p>
        Hoa: <input type="text" name="hoa">
        <p><?php echo $error3; ?></p>
        <input type="submit" name="submit" value="Click">
    </form>
</body>
</html>