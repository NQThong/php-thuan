<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        $error1 = '';
        $error2 = '';
        $error3 = '';
        $error4 = '';
        $error5 = '';
        $error6 = '';
        $result = '';
        if(isset($_POST['submit'])){
            $demo = true;
            if(empty($_POST['toan'])){
                $error1 = 'Nhap toan';
                $demo = false;
            }
            if(empty($_POST['ly'])){
                $error2 = 'Nhap ly';
                $demo = false;
            }
            if(empty($_POST['hoa'])){
                $error3 = 'Nhap hoa';
                $demo = false;
            }
            if(empty($_POST['tienganh'])){
                $error4 = 'Nhap tienganh';
                $demo = false;
            }
            if(empty($_POST['van'])){
                $error5 = 'Nhap van';
                $demo = false;
            }
            if(empty($_POST['lichsu'])){
                $error6 = 'Nhap lichsu';
                $demo = false;
            }
        }

        if($demo == true){
            // echo "1";
            $toan = $_POST['toan'];
            $ly = $_POST['ly'];
            $hoa = $_POST['hoa'];
            $tienganh = $_POST['tienganh'];
            $van = $_POST['van'];
            $lichsu = $_POST['lichsu'];
            if((($toan >= 0) && ($toan <= 10)) && (($ly >= 0) && ($ly <= 10)) && (($hoa >= 0) && ($hoa <= 10)) && 
            (($tienganh >= 0) && ($tienganh <= 10)) && (($van >= 0) && ($van <= 10)) && (($lichsu >= 0) && ($lichsu <= 10))){
                if(($toan < 4) || ($ly < 4) || ($hoa < 4) || ($tienganh < 4) || ($van < 4) || ($lichsu < 4)){
                    echo 'Mot trong 6 mon duoi 4 diem';
                }else{
                    $trungbinh = ($toan + $ly + $hoa + $tienganh + $van + $lichsu) / 6;
                    if($trungbinh < 5){
                        $result = 'Yeu';
                        // echo 'Yeu';
                    }elseif(($trungbinh >= 5) && ($trungbinh <= 6.4)){
                        $result = 'Trung binh';
                        // echo 'Trung binh';
                    }elseif(($trungbinh >= 6.5) && ($trungbinh <= 7.9)){
                        $result = 'Kha';
                    }elseif(($trungbinh >= 7.9)){
                        $result = 'Gioi';
                    }
                }
            }else{
                echo 'Diem gioi han tu 0 den 10';
            }
        }

    ?>
    <form action="" method="POST">
        Toan: <input type="text" name="toan">
        <p><?php echo $error1; ?></p>
        ly: <input type="text" name="ly">
        <p><?php echo $error2; ?></p>
        Hoa: <input type="text" name="hoa">
        <p><?php echo $error3; ?></p>
        Tieng Anh: <input type="text" name="tienganh">
        <p><?php echo $error4; ?></p>
        Van: <input type="text" name="van">
        <p><?php echo $error5; ?></p>
        Lich Su: <input type="text" name="lichsu">
        <p><?php echo $error6; ?></p>
        <input type="submit" name="submit" value="Click">
        <p><?php echo $result; ?></p>
    </form>
</body>
</html>